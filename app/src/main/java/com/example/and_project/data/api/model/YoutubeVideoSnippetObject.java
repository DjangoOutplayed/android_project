package com.example.and_project.data.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class YoutubeVideoSnippetObject {
    private String title;
    private String description;
    private String channelTitle;
    private Date publishedAt;
    @SerializedName("thumbnails")
    private YoutubeVideoThumbnailObject youtubeVideoThumbnailObject;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getChannelTitle() {
        return channelTitle;
    }

    public String getThumbnailUrl() {
        return youtubeVideoThumbnailObject.getDefaultThumbnail().getUrl();
    }

    public String getHighThumbnailUrl() {
        return youtubeVideoThumbnailObject.getHighThumbnail().getUrl();
    }

    public Date getPublishedAt() {
        return publishedAt;
    }
}
