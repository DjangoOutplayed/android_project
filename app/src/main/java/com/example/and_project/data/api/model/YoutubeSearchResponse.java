package com.example.and_project.data.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Gson object that represents the response produced by the request used to search
 * through youtube videos (https://developers.google.com/youtube/v3/docs/search/list).
 */
public class YoutubeSearchResponse {
    @SerializedName("items")
    private List<YoutubeVideo> youtubeVideos;

    public List<YoutubeVideo> getYoutubeVideos() {
        return youtubeVideos;
    }
}
