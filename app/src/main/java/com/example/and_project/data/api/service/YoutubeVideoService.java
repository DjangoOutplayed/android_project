package com.example.and_project.data.api.service;

import com.example.and_project.data.api.model.YoutubeDetailsResponse;
import com.example.and_project.data.api.model.YoutubeSearchResponse;

import java.util.Map;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Interface regrouping all request available to Youtube Api through Retrofit service
 */
public interface YoutubeVideoService {

    /**
     * Search through youtube videos with keywords (https://developers.google.com/youtube/v3/docs/search/list)
     *
     * @param queryParams
     * @return
     */
    @GET("search")
    Single<YoutubeSearchResponse> getYoutubeVideoSearchResponse(@QueryMap Map<String, String> queryParams);

    /**
     * Get a video details from youtube api (https://developers.google.com/youtube/v3/docs/videos/list)
     *
     * @param queryParams
     * @return
     */
    @GET("videos")
    Single<YoutubeDetailsResponse> getYoutubeVideoDetails(@QueryMap Map<String, String> queryParams);
}
