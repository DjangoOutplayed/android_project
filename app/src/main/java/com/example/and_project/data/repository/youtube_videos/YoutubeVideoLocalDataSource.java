package com.example.and_project.data.repository.youtube_videos;

import com.example.and_project.data.db.AppDatabase;
import com.example.and_project.data.db.entities.YoutubeVideoEntity;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

public class YoutubeVideoLocalDataSource {

    private static AppDatabase localDb;

    public YoutubeVideoLocalDataSource(AppDatabase localDb) {
        YoutubeVideoLocalDataSource.localDb = localDb;
    }

    public Flowable<List<YoutubeVideoEntity>> getAllWatchLaterVideos() {
        return localDb.youtubeVideoDao().getAllWatchLaterVideos();
    }

    public Single<YoutubeVideoEntity> getWatchLaterVideoById(String id) {
        return localDb.youtubeVideoDao().getWatchLaterVideoById(id);
    }

    public Completable addVideoToWatchLater(YoutubeVideoEntity youtubeVideoEntity) {
        return localDb.youtubeVideoDao().addVideoToWatchLater(youtubeVideoEntity);
    }

    public Completable removeVideoFromWatchLaterById(String id) {
        return localDb.youtubeVideoDao().removeVideoFromWatchLaterById(id);
    }
}
