package com.example.and_project.data.api.model;

public class YoutubeVideoStatisticsObject {
    private long viewCount;
    private long likeCount;
    private long dislikeCount;
    private long commentCount;

    public long getViewCount() {
        return viewCount;
    }

    public long getLikeCount() {
        return likeCount;
    }

    public long getDislikeCount() {
        return dislikeCount;
    }

    public long getCommentCount() {
        return commentCount;
    }
}
