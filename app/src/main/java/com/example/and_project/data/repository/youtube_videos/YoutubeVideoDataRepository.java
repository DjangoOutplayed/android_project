package com.example.and_project.data.repository.youtube_videos;

import com.example.and_project.data.api.model.YoutubeDetailsResponse;
import com.example.and_project.data.api.model.YoutubeSearchResponse;
import com.example.and_project.data.db.entities.YoutubeVideoEntity;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * Class regrouping all methods that interacts with application data, local or remote.
 */
public class YoutubeVideoDataRepository {
    private static YoutubeVideoRemoteDataSource remoteDataSource;
    private static YoutubeVideoLocalDataSource localDataSource;

    public YoutubeVideoDataRepository(
            YoutubeVideoRemoteDataSource remoteDataSource,
            YoutubeVideoLocalDataSource localDataSource
    ) {
        YoutubeVideoDataRepository.remoteDataSource = remoteDataSource;
        YoutubeVideoDataRepository.localDataSource = localDataSource;
    }

    public Single<YoutubeSearchResponse> getSearchVideos(String keywords) {
        return remoteDataSource.getSearchVideos(keywords);
    }

    public Single<YoutubeDetailsResponse> getVideoDetails(String id) {
        return remoteDataSource.getVideoDetails(id);
    }

    public Flowable<List<YoutubeVideoEntity>> getAllWatchLaterVideos() {
        return localDataSource.getAllWatchLaterVideos();
    }

    public Single<YoutubeVideoEntity> getWatchLaterVideoById(String id) {
        return localDataSource.getWatchLaterVideoById(id);
    }

    public Completable addVideoToWatchLater(YoutubeVideoEntity youtubeVideoEntity) {
        return localDataSource.addVideoToWatchLater(youtubeVideoEntity);
    }

    public Completable removeVideoFromWatchLaterById(String id) {
        return localDataSource.removeVideoFromWatchLaterById(id);
    }
}
