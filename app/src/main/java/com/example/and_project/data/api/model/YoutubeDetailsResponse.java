package com.example.and_project.data.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Gson object that represents the response produced by the request used to get
 * a youtube video details (https://developers.google.com/youtube/v3/docs/videos/list).
 */
public class YoutubeDetailsResponse {
    @SerializedName("items")
    private List<YoutubeVideoWithOneId> youtubeVideos;

    public YoutubeVideoWithOneId getYoutubeVideoDetails() {
        if (!youtubeVideos.isEmpty()) {
            return youtubeVideos.get(0);
        } else {
            return null;
        }
    }
}
