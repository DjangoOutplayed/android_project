package com.example.and_project.data.api.model;

import com.google.gson.annotations.SerializedName;

public class YoutubeVideoThumbnailObject {
    @SerializedName("default")
    private YoutubeVideoDefaultThumbnail defaultThumbnail;

    @SerializedName("high")
    private YoutubeVideoHighThumbnail highThumbnail;

    public YoutubeVideoDefaultThumbnail getDefaultThumbnail() {
        return defaultThumbnail;
    }

    public YoutubeVideoHighThumbnail getHighThumbnail() {
        return highThumbnail;
    }
}
