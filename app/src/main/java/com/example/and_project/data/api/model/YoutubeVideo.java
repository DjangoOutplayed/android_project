package com.example.and_project.data.api.model;

import com.google.gson.annotations.SerializedName;

public class YoutubeVideo implements YoutubeApiObject {
    @SerializedName("id")
    private YoutubeVideoIdObject videoIds;
    @SerializedName("snippet")
    private YoutubeVideoSnippetObject snippetObject;
    @SerializedName("contentDetails")
    private YoutubeVideoContentDetailsObject contentDetailsObject;
    @SerializedName("statistics")
    private YoutubeVideoStatisticsObject statisticsObject;

    public String getId() {
        return videoIds.getVideoId();
    }

    public YoutubeVideoIdObject getVideoIds() {
        return videoIds;
    }

    public YoutubeVideoSnippetObject getSnippetObject() {
        return snippetObject;
    }

    public YoutubeVideoContentDetailsObject getContentDetailsObject() {
        return contentDetailsObject;
    }

    public YoutubeVideoStatisticsObject getStatisticsObject() {
        return statisticsObject;
    }
}
