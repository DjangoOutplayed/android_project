package com.example.and_project.data.repository.youtube_videos;

import com.example.and_project.YoutubeSearcherApplication;
import com.example.and_project.data.api.model.YoutubeDetailsResponse;
import com.example.and_project.data.api.model.YoutubeSearchResponse;
import com.example.and_project.data.api.service.YoutubeVideoService;

import java.util.Map;

import io.reactivex.Single;

public class YoutubeVideoRemoteDataSource {
    private static YoutubeVideoService youtubeVideoService;

    public YoutubeVideoRemoteDataSource(YoutubeVideoService youtubeVideoService) {
        YoutubeVideoRemoteDataSource.youtubeVideoService = youtubeVideoService;
    }

    public Single<YoutubeSearchResponse> getSearchVideos(String keywords) {
        Map<String, String> queryParams = YoutubeSearcherApplication.BASE_SEARCH_QUERY;
        queryParams.put("q", keywords);
        queryParams.put("part", "snippet");
        queryParams.put("maxResults", "20");
        queryParams.put("type", "video");

        return youtubeVideoService.getYoutubeVideoSearchResponse(queryParams);
    }

    public Single<YoutubeDetailsResponse> getVideoDetails(String id) {
        Map<String, String> queryParams = YoutubeSearcherApplication.BASE_SEARCH_QUERY;
        queryParams.put("part", "snippet,contentDetails,statistics");
        queryParams.put("id", id);

        return youtubeVideoService.getYoutubeVideoDetails(queryParams);
    }
}
