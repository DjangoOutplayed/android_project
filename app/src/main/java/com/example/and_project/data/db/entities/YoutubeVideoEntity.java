package com.example.and_project.data.db.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Main and only entity used in local database. Fields were chosen based on whether they'd be useful
 * or not in watch later section.
 */
@Entity
public class YoutubeVideoEntity {
    @NonNull
    @PrimaryKey
    private String id;
    private String videoTitle;
    private String channelTitle;
    private String thumbnailUrl;
    private String highThumbnailUrl;

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getChannelTitle() {
        return channelTitle;
    }

    public void setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getHighThumbnailUrl() {
        return highThumbnailUrl;
    }

    public void setHighThumbnailUrl(String highThumbnailUrl) {
        this.highThumbnailUrl = highThumbnailUrl;
    }
}
