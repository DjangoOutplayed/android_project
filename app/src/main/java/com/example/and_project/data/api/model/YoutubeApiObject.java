package com.example.and_project.data.api.model;

/**
 * Interface used to regroup YoutubeVideo and YoutubeVideoWithOneId,
 * as they share everything except the way they provide the youtube video ID.
 * This difference is abstracted under the getId() method.
 */
public interface YoutubeApiObject {
    String getId();

    YoutubeVideoSnippetObject getSnippetObject();

    YoutubeVideoContentDetailsObject getContentDetailsObject();

    YoutubeVideoStatisticsObject getStatisticsObject();
}
