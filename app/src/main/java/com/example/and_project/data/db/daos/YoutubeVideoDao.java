package com.example.and_project.data.db.daos;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.and_project.data.db.entities.YoutubeVideoEntity;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * Interface regrouping all available queries to local Room database.
 */
@Dao
public interface YoutubeVideoDao {
    @Query("SELECT * FROM YoutubeVideoEntity")
    Flowable<List<YoutubeVideoEntity>> getAllWatchLaterVideos();

    @Query("SELECT * FROM YoutubeVideoEntity WHERE id = :id")
    Single<YoutubeVideoEntity> getWatchLaterVideoById(String id);

    @Insert
    Completable addVideoToWatchLater(YoutubeVideoEntity youtubeVideoEntity);

    @Query("DELETE FROM YoutubeVideoEntity WHERE id = :id")
    Completable removeVideoFromWatchLaterById(String id);
}
