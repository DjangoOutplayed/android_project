package com.example.and_project.data.di;

import android.content.Context;

import androidx.room.Room;

import com.example.and_project.data.api.service.YoutubeVideoService;
import com.example.and_project.data.db.AppDatabase;
import com.example.and_project.data.repository.youtube_videos.YoutubeVideoDataRepository;
import com.example.and_project.data.repository.youtube_videos.YoutubeVideoLocalDataSource;
import com.example.and_project.data.repository.youtube_videos.YoutubeVideoRemoteDataSource;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Class simulating Dependency injection for several key components of the application.
 */
public class FakeDependencyInjection {
    private static Context applicationContext;
    private static Retrofit retrofit;
    private static Gson gson;
    private static YoutubeVideoService youtubeVideoService;
    private static YoutubeVideoRemoteDataSource youtubeVideoRemoteDataSource;
    private static YoutubeVideoLocalDataSource youtubeVideoLocalDataSource;
    private static YoutubeVideoDataRepository youtubeVideoDataRepository;
    private static AppDatabase localDb;

    public static YoutubeVideoDataRepository getYoutubeVideoDataRepository() {
        if (youtubeVideoDataRepository == null) {
            youtubeVideoDataRepository = new YoutubeVideoDataRepository(
                    getYoutubeVideoRemoteDataSource(),
                    getYoutubeVideoLocalDataSource()
            );
        }

        return youtubeVideoDataRepository;
    }

    private static YoutubeVideoRemoteDataSource getYoutubeVideoRemoteDataSource() {
        if (youtubeVideoRemoteDataSource == null) {
            youtubeVideoRemoteDataSource = new YoutubeVideoRemoteDataSource(getYoutubeVideoService());
        }

        return youtubeVideoRemoteDataSource;
    }

    private static YoutubeVideoLocalDataSource getYoutubeVideoLocalDataSource() {
        if (youtubeVideoLocalDataSource == null) {
            youtubeVideoLocalDataSource = new YoutubeVideoLocalDataSource(getLocalDb());
        }

        return youtubeVideoLocalDataSource;
    }

    private static YoutubeVideoService getYoutubeVideoService() {
        if (youtubeVideoService == null) {
            youtubeVideoService = getRetrofit().create(YoutubeVideoService.class);
        }

        return youtubeVideoService;
    }

    private static AppDatabase getLocalDb() {
        if (localDb == null) {
            localDb = Room.databaseBuilder(applicationContext,
                    AppDatabase.class, "local-database").build();

        }

        return localDb;
    }

    public static Retrofit getRetrofit() {
        if (retrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .addNetworkInterceptor(new StethoInterceptor())
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl("https://www.googleapis.com/youtube/v3/")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(getGson()))
                    .build();
        }
        return retrofit;
    }

    private static Gson getGson() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }

    public static void setContext(Context context) {
        applicationContext = context;
    }
}
