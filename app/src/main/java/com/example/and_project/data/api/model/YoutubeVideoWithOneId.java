package com.example.and_project.data.api.model;

import com.google.gson.annotations.SerializedName;

public class YoutubeVideoWithOneId implements YoutubeApiObject {
    private String id;
    @SerializedName("snippet")
    private YoutubeVideoSnippetObject snippetObject;
    @SerializedName("contentDetails")
    private YoutubeVideoContentDetailsObject contentDetailsObject;
    @SerializedName("statistics")
    private YoutubeVideoStatisticsObject statisticsObject;

    public String getId() {
        return id;
    }

    public YoutubeVideoSnippetObject getSnippetObject() {
        return snippetObject;
    }

    public YoutubeVideoContentDetailsObject getContentDetailsObject() {
        return contentDetailsObject;
    }

    public YoutubeVideoStatisticsObject getStatisticsObject() {
        return statisticsObject;
    }
}
