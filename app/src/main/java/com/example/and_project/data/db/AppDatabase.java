package com.example.and_project.data.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.and_project.data.db.daos.YoutubeVideoDao;
import com.example.and_project.data.db.entities.YoutubeVideoEntity;

/**
 * Bootstrap class for Room local database
 */
@Database(entities = {YoutubeVideoEntity.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract YoutubeVideoDao youtubeVideoDao();
}

