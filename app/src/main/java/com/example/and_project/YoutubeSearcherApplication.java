package com.example.and_project;

import android.app.Application;

import com.example.and_project.data.di.FakeDependencyInjection;
import com.facebook.stetho.Stetho;

import java.util.HashMap;
import java.util.Map;

public class YoutubeSearcherApplication extends Application {
    public static final String API_KEY = "AIzaSyBZ3TqvKB5qxJ1ksb2Ya1U3mXzi0406HX4";

    public static Map<String, String> BASE_SEARCH_QUERY = new HashMap<>();

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
        FakeDependencyInjection.setContext(this);
        BASE_SEARCH_QUERY.put("key", API_KEY);
        BASE_SEARCH_QUERY.put("regionCode", "fr");
    }
}
