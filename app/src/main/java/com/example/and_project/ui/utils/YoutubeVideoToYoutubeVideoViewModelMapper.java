package com.example.and_project.ui.utils;

import android.text.Html;

import com.example.and_project.data.api.model.YoutubeApiObject;
import com.example.and_project.data.api.model.YoutubeVideoContentDetailsObject;
import com.example.and_project.data.api.model.YoutubeVideoStatisticsObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Mapper from YoutubeVideo (youtube api) to YoutubeVideoViewModel (recycler view model),
 * hence works with any of the object implementing the YoutubeApiObject interface.
 *
 * @param <T> must implement YoutubeApiObject interface
 */
public class YoutubeVideoToYoutubeVideoViewModelMapper<T extends YoutubeApiObject> {


    public YoutubeVideoViewModel map(T video) {
        YoutubeVideoViewModel youtubeVideoViewModel = new YoutubeVideoViewModel();

        String title = Html.fromHtml(video.getSnippetObject().getTitle()).toString();
        String channelTitle = Html.fromHtml(video.getSnippetObject().getChannelTitle()).toString();

        youtubeVideoViewModel.setVideoTitle(title);
        youtubeVideoViewModel.setVideoChannel(channelTitle);
        youtubeVideoViewModel.setVideoThumbnailUrl(video.getSnippetObject().getThumbnailUrl());
        youtubeVideoViewModel.setVideoHighThumbnailUrl(video.getSnippetObject().getHighThumbnailUrl());
        youtubeVideoViewModel.setVideoYoutubeId(video.getId());
        youtubeVideoViewModel.setPublishedAt(video.getSnippetObject().getPublishedAt());

        YoutubeVideoContentDetailsObject contentDetailsObject = video.getContentDetailsObject();
        YoutubeVideoStatisticsObject statisticsObject = video.getStatisticsObject();

        if (contentDetailsObject != null) {
            youtubeVideoViewModel.setDuration(contentDetailsObject.getDuration());
        }

        if (statisticsObject != null) {
            youtubeVideoViewModel.setCommentCount(statisticsObject.getCommentCount());
            youtubeVideoViewModel.setDislikeCount(statisticsObject.getDislikeCount());
            youtubeVideoViewModel.setLikeCount(statisticsObject.getLikeCount());
            youtubeVideoViewModel.setViewCount(statisticsObject.getViewCount());
        }

        return youtubeVideoViewModel;
    }

    public List<YoutubeVideoViewModel> map(List<T> youtubeVideoList) {
        List<YoutubeVideoViewModel> youtubeVideoViewModelList = new ArrayList<>();
        for (T video : youtubeVideoList) {
            youtubeVideoViewModelList.add(map(video));
        }

        return youtubeVideoViewModelList;
    }
}
