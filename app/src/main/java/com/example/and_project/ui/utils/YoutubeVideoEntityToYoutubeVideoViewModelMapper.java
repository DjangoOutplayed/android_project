package com.example.and_project.ui.utils;

import android.text.Html;

import com.example.and_project.data.db.entities.YoutubeVideoEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Mapper from YoutubeVideoEntity (local database) to YoutubeVideoViewModel (recycler view model)
 */
public class YoutubeVideoEntityToYoutubeVideoViewModelMapper {

    public YoutubeVideoViewModel map(YoutubeVideoEntity video) {
        YoutubeVideoViewModel youtubeVideoViewModel = new YoutubeVideoViewModel();

        String title = Html.fromHtml(video.getVideoTitle()).toString();
        String channelTitle = Html.fromHtml(video.getChannelTitle()).toString();

        youtubeVideoViewModel.setVideoTitle(title);
        youtubeVideoViewModel.setVideoChannel(channelTitle);
        youtubeVideoViewModel.setVideoThumbnailUrl(video.getThumbnailUrl());
        youtubeVideoViewModel.setVideoHighThumbnailUrl(video.getHighThumbnailUrl());
        youtubeVideoViewModel.setVideoYoutubeId(video.getId());

        return youtubeVideoViewModel;
    }

    public List<YoutubeVideoViewModel> map(List<YoutubeVideoEntity> youtubeVideoList) {
        List<YoutubeVideoViewModel> youtubeVideoViewModelList = new ArrayList<>();
        for (YoutubeVideoEntity video : youtubeVideoList) {
            youtubeVideoViewModelList.add(map(video));
        }

        return youtubeVideoViewModelList;
    }
}
