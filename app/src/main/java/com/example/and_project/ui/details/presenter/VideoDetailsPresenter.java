package com.example.and_project.ui.details.presenter;

import com.example.and_project.data.db.entities.YoutubeVideoEntity;
import com.example.and_project.data.di.FakeDependencyInjection;
import com.example.and_project.data.repository.youtube_videos.YoutubeVideoDataRepository;
import com.example.and_project.ui.details.YoutubeVideoDetailsContract;
import com.example.and_project.ui.utils.YoutubeVideoViewModel;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class VideoDetailsPresenter implements YoutubeVideoDetailsContract.Presenter {
    public YoutubeVideoDetailsContract.View view;
    private CompositeDisposable cd;
    public YoutubeVideoDataRepository repository;

    public VideoDetailsPresenter() {
        cd = new CompositeDisposable();
        repository = FakeDependencyInjection.getYoutubeVideoDataRepository();
    }

    public void attachview(YoutubeVideoDetailsContract.View view) {
        this.view = view;
    }

    public void detachView() {
        this.view = null;
        cd.dispose();
    }

    public void cancelSubscription() {
        cd.clear();
    }

    public void setIconWithWatchLaterState(String youtubeVideoId) {
        Single<YoutubeVideoEntity> response = repository.getWatchLaterVideoById(youtubeVideoId);
        Disposable disposable = response
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<YoutubeVideoEntity>() {
                    @Override
                    public void onSuccess(YoutubeVideoEntity youtubeVideoEntity) {
                        view.toggleIconOnFavButton(true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.toggleIconOnFavButton(false);
                        System.out.println(e.getMessage());
                        for (StackTraceElement element : e.getStackTrace()
                        ) {
                            System.out.println(element);
                        }
                    }
                });

        cd.add(disposable);
    }

    public void toggleWatchLaterState(final boolean isWatchLater, YoutubeVideoViewModel youtubeVideoViewModel) {
        Completable response;
        if (isWatchLater) {
            response = repository.removeVideoFromWatchLaterById(youtubeVideoViewModel.getVideoYoutubeId());
        } else {
            YoutubeVideoEntity entity = new YoutubeVideoEntity();
            entity.setId(youtubeVideoViewModel.getVideoYoutubeId());
            entity.setVideoTitle(youtubeVideoViewModel.getVideoTitle());
            entity.setChannelTitle(youtubeVideoViewModel.getVideoChannel());
            entity.setThumbnailUrl(youtubeVideoViewModel.getVideoThumbnailUrl());
            entity.setHighThumbnailUrl(youtubeVideoViewModel.getVideoHighThumbnailUrl());

            response = repository.addVideoToWatchLater(entity);
        }

        Disposable disposable = response
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        view.toggleIconOnFavButton(!isWatchLater);
                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println(e.getMessage());
                        for (StackTraceElement element : e.getStackTrace()
                        ) {
                            System.out.println(element);
                        }
                    }
                });
    }
}
