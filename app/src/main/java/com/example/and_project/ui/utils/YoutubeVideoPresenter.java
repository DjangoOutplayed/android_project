package com.example.and_project.ui.utils;

import com.example.and_project.data.api.model.YoutubeDetailsResponse;
import com.example.and_project.data.api.model.YoutubeSearchResponse;
import com.example.and_project.data.api.model.YoutubeVideoWithOneId;
import com.example.and_project.data.di.FakeDependencyInjection;
import com.example.and_project.data.repository.youtube_videos.YoutubeVideoDataRepository;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Presenter used by List view and Grid View.
 */
public class YoutubeVideoPresenter implements YoutubeVideoSearchContract.Presenter {
    public YoutubeVideoSearchContract.View view;
    private CompositeDisposable cd;
    public YoutubeVideoDataRepository repository;

    public YoutubeVideoPresenter() {
        cd = new CompositeDisposable();
        repository = FakeDependencyInjection.getYoutubeVideoDataRepository();
    }

    public void attachview(YoutubeVideoSearchContract.View view) {
        this.view = view;
    }

    public void detachView() {
        this.view = null;
        cd.dispose();
    }

    public void cancelSubscription() {
        cd.clear();
    }

    public void searchVideos(String keywords) {
        Single<YoutubeSearchResponse> youtubeVideoSingle = repository.getSearchVideos(keywords);

        final YoutubeVideoToYoutubeVideoViewModelMapper mapper = new YoutubeVideoToYoutubeVideoViewModelMapper();

        Disposable disposable = youtubeVideoSingle
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<YoutubeSearchResponse>() {
                    @Override
                    public void onSuccess(YoutubeSearchResponse youtubeSearchResponse) {
                        List<YoutubeVideoViewModel> mappedResponse = mapper.map(youtubeSearchResponse.getYoutubeVideos());
                        view.displayVideos(mappedResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println(e.getMessage());
                        for (StackTraceElement element : e.getStackTrace()
                        ) {
                            System.out.println(element);
                        }
                    }
                });

        cd.add(disposable);
    }

    public void getDetailsOnClicked(String youtubeVideoId) {
        Single<YoutubeDetailsResponse> youtubeVideoSingle = repository.getVideoDetails(youtubeVideoId);

        final YoutubeVideoToYoutubeVideoViewModelMapper mapper = new YoutubeVideoToYoutubeVideoViewModelMapper();

        Disposable disposable = youtubeVideoSingle
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<YoutubeDetailsResponse>() {
                    @Override
                    public void onSuccess(YoutubeDetailsResponse youtubeDetailsResponse) {
                        YoutubeVideoWithOneId youtubeVideo = youtubeDetailsResponse.getYoutubeVideoDetails();
                        YoutubeVideoViewModel youtubeVideoViewModel = mapper.map(youtubeVideo);

                        view.goToDetailsView(youtubeVideoViewModel);
                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println(e.getMessage());
                        for (StackTraceElement element : e.getStackTrace()
                        ) {
                            System.out.println(element);
                        }
                    }
                });
    }
}
