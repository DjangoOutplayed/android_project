package com.example.and_project.ui.watch_later.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.and_project.R;
import com.example.and_project.ui.utils.YoutubeVideoElementAction;
import com.example.and_project.ui.utils.YoutubeVideoViewModel;

import java.util.ArrayList;
import java.util.List;

public class WatchLaterAdapter extends RecyclerView.Adapter<WatchLaterAdapter.WatchLaterHolder> {
    public static class WatchLaterHolder extends RecyclerView.ViewHolder {
        private TextView videoTitleTextView;
        private TextView videoChannelTextView;
        private ImageView videoIconImageView;
        private View v;
        private YoutubeVideoViewModel youtubeVideoViewModel;
        private YoutubeVideoElementAction youtubeVideoElementAction;

        public WatchLaterHolder(View v, YoutubeVideoElementAction youtubeVideoElementAction) {
            super(v);
            this.v = v;
            videoTitleTextView = v.findViewById(R.id.video_title_textview);
            videoChannelTextView = v.findViewById(R.id.video_channel_textview);
            videoIconImageView = v.findViewById(R.id.video_image_preview);
            this.youtubeVideoElementAction = youtubeVideoElementAction;
            setupListeners();
        }

        private void setupListeners() {
            v.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    youtubeVideoElementAction.onVideoClicked(youtubeVideoViewModel.getVideoYoutubeId());
                }
            });
        }

        void bind(YoutubeVideoViewModel youtubeVideoViewModel) {
            this.youtubeVideoViewModel = youtubeVideoViewModel;
            videoTitleTextView.setText(youtubeVideoViewModel.getVideoTitle());
            videoChannelTextView.setText(youtubeVideoViewModel.getVideoChannel());
            Glide.with(v)
                    .load(youtubeVideoViewModel.getVideoThumbnailUrl())
                    .into(videoIconImageView);
        }
    }

    private List<YoutubeVideoViewModel> youtubeVideoViewModelList;
    private YoutubeVideoElementAction youtubeVideoElementAction;

    public WatchLaterAdapter(YoutubeVideoElementAction youtubeVideoElementAction) {
        this.youtubeVideoViewModelList = new ArrayList<>();
        this.youtubeVideoElementAction = youtubeVideoElementAction;
    }

    public void bindViewModels(List<YoutubeVideoViewModel> youtubeVideoViewModels) {
        this.youtubeVideoViewModelList.clear();
        this.youtubeVideoViewModelList.addAll(youtubeVideoViewModels);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public WatchLaterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_view, parent, false);
        return new WatchLaterHolder(v, youtubeVideoElementAction);
    }

    @Override
    public void onBindViewHolder(@NonNull WatchLaterHolder holder, int position) {
        holder.bind(youtubeVideoViewModelList.get(position));
    }

    @Override
    public int getItemCount() {
        return youtubeVideoViewModelList.size();
    }

    public void clear() {
        youtubeVideoViewModelList.clear();
    }
}
