package com.example.and_project.ui.utils;

/**
 * Interface used to keep state of searched string between List view and grid view.
 * Implemented by MainActivity, methods used by list and grid fragment.
 */
public interface SameSearchActivity {
    String getSearchKeywords();

    void setSearchKeywords(String s);
}
