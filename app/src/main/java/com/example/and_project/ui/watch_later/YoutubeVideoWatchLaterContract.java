package com.example.and_project.ui.watch_later;

import com.example.and_project.ui.utils.YoutubeVideoViewModel;

import java.util.List;

public interface YoutubeVideoWatchLaterContract {
    interface Presenter {
        void attachview(View view);

        void detachView();

        void cancelSubscription();

        void getWatchLaterVideos();

        void getDetailsOnClicked(String youtubeVideoId);
    }

    interface View {
        void displayVideos(List<YoutubeVideoViewModel> listViewViewModelList);

        void goToDetailsView(YoutubeVideoViewModel youtubeVideoViewModel);
    }
}
