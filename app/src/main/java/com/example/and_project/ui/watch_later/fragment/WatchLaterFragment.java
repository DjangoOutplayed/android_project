package com.example.and_project.ui.watch_later.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.and_project.R;
import com.example.and_project.ui.details.fragment.VideoDetailsFragment;
import com.example.and_project.ui.utils.YoutubeVideoElementAction;
import com.example.and_project.ui.utils.YoutubeVideoViewModel;
import com.example.and_project.ui.watch_later.YoutubeVideoWatchLaterContract;
import com.example.and_project.ui.watch_later.adapter.WatchLaterAdapter;
import com.example.and_project.ui.watch_later.presenter.YoutubeVideoWatchLaterPresenter;

import java.util.List;

public class WatchLaterFragment extends Fragment implements YoutubeVideoWatchLaterContract.View, YoutubeVideoElementAction {
    private View rootView;
    private YoutubeVideoWatchLaterContract.Presenter presenter;
    private RecyclerView recyclerView;
    private WatchLaterAdapter watchLaterAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_watch_later, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupRecyclerView();
        presenter = new YoutubeVideoWatchLaterPresenter();
        presenter.attachview(this);
        presenter.getWatchLaterVideos();
    }

    private void setupRecyclerView() {
        recyclerView = rootView.findViewById(R.id.recycler_view);
        watchLaterAdapter = new WatchLaterAdapter(this);
        recyclerView.setAdapter(watchLaterAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    public void displayVideos(List<YoutubeVideoViewModel> listViewViewModelList) {
        watchLaterAdapter.bindViewModels(listViewViewModelList);
    }

    public void onVideoClicked(String youtubeVideoId) {
        presenter.getDetailsOnClicked(youtubeVideoId);
    }

    public void goToDetailsView(YoutubeVideoViewModel youtubeVideoViewModel) {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment f = VideoDetailsFragment.newInstance(youtubeVideoViewModel);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.nav_host_fragment, f);
        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();
    }
}