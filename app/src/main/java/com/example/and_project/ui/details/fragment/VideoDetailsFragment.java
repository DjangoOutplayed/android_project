package com.example.and_project.ui.details.fragment;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.and_project.R;
import com.example.and_project.ui.details.YoutubeVideoDetailsContract;
import com.example.and_project.ui.details.presenter.VideoDetailsPresenter;
import com.example.and_project.ui.utils.YoutubeVideoViewModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.Locale;


public class VideoDetailsFragment extends Fragment implements YoutubeVideoDetailsContract.View {
    private View rootView;
    private YoutubeVideoViewModel youtubeVideoViewModel;
    private YoutubeVideoDetailsContract.Presenter presenter;
    private boolean isWatchLater;

    private String videoThumbnailUrl;
    private String videoTitle;
    private String videoChannel;
    private Date publishedAt;
    private String duration;
    private long viewCount;
    private long likeCount;
    private long dislikeCount;
    private long commentCount;

    private TextView videoTitleView;
    private TextView videoChannelView;
    private TextView videoChannelPrefixView;
    private TextView publishedAtView;
    private TextView durationView;
    private TextView viewCountView;
    private TextView likeCountView;
    private TextView dislikeCountView;
    private TextView commentCountView;
    private ImageView videoThumbnailView;
    private ImageView favButtonView;

    public static VideoDetailsFragment newInstance(YoutubeVideoViewModel youtubeVideoViewModel) {
        return new VideoDetailsFragment(youtubeVideoViewModel);
    }

    public VideoDetailsFragment(YoutubeVideoViewModel youtubeVideoViewModel) {
        this.youtubeVideoViewModel = youtubeVideoViewModel;
        presenter = new VideoDetailsPresenter();

        videoThumbnailUrl = youtubeVideoViewModel.getVideoHighThumbnailUrl();
        videoTitle = youtubeVideoViewModel.getVideoTitle();
        videoChannel = youtubeVideoViewModel.getVideoChannel();
        publishedAt = youtubeVideoViewModel.getPublishedAt();
        duration = youtubeVideoViewModel.getDuration();
        viewCount = youtubeVideoViewModel.getViewCount();
        likeCount = youtubeVideoViewModel.getLikeCount();
        dislikeCount = youtubeVideoViewModel.getDislikeCount();
        commentCount = youtubeVideoViewModel.getCommentCount();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.video_details_view, container, false);

        bindViewElements();
        populatingData();
        presenter.attachview(this);

        return rootView;
    }

    private void bindViewElements() {
        videoTitleView = rootView.findViewById(R.id.video_title_textview);
        videoChannelView = rootView.findViewById(R.id.video_channel_textview);
        videoChannelPrefixView = rootView.findViewById(R.id.video_channel_prefix);
        publishedAtView = rootView.findViewById(R.id.video_publishedAt_textview);
        durationView = rootView.findViewById(R.id.video_duration_textview);
        viewCountView = rootView.findViewById(R.id.video_viewCount_textview);
        likeCountView = rootView.findViewById(R.id.video_likeCount_textview);
        dislikeCountView = rootView.findViewById(R.id.video_dislikeCount_textview);
        commentCountView = rootView.findViewById(R.id.video_commentCount_textview);
        videoThumbnailView = rootView.findViewById(R.id.video_thumbnail_imageview);
        favButtonView = rootView.findViewById(R.id.fav_button_imageview);
    }

    private void populatingData() {
        videoTitleView.setText(videoTitle);
        videoTitleView.setTextColor(0xFF000000);

        videoChannelPrefixView.setText(R.string.channel_prefix);
        videoChannelView.setText(videoChannel);
        videoChannelView.setTextColor(0xFF000000);

        DateFormat formatter = new SimpleDateFormat("dd MMM. yyyy", Locale.ENGLISH);
        String publishedAt = formatter.format(this.publishedAt);
        publishedAtView.setText(publishedAt + " • ");

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            Duration d = Duration.parse(duration);
            durationView.setText(formatDuration(d));
        } else {
            durationView.setText("");
            publishedAtView.setText(publishedAt);
        }

        viewCountView.setText(viewCount + " views • ");
        likeCountView.setText(likeCount + " likes - ");
        dislikeCountView.setText(dislikeCount + " dislikes");
        commentCountView.setText(commentCount + " comments");

        Glide.with(rootView)
                .load(videoThumbnailUrl)
                .into(videoThumbnailView);

        presenter.setIconWithWatchLaterState(youtubeVideoViewModel.getVideoYoutubeId());
        favButtonView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                presenter.toggleWatchLaterState(isWatchLater, youtubeVideoViewModel);
            }
        });
    }

    public void toggleIconOnFavButton(boolean isWatchLater) {
        if (isWatchLater) {
            favButtonView.setImageResource(R.drawable.ic_fav_button_enabled_foreground);
        } else {
            favButtonView.setImageResource(R.drawable.ic_fav_button_disabled_foreground);
        }

        this.isWatchLater = isWatchLater;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static String formatDuration(Duration duration) {
        long seconds = duration.getSeconds();
        long absSeconds = Math.abs(seconds);
        String positive = String.format(
                "%d:%02d:%02d",
                absSeconds / 3600,
                (absSeconds % 3600) / 60,
                absSeconds % 60);
        return seconds < 0 ? "-" + positive : positive;
    }
}
