package com.example.and_project.ui.utils;

import java.util.List;

/**
 * Contract used by List view and Grid view features.
 */
public interface YoutubeVideoSearchContract {
    interface Presenter {
        void attachview(View view);

        void detachView();

        void cancelSubscription();

        void searchVideos(String keywords);

        void getDetailsOnClicked(String youtubeVideoId);
    }

    interface View {
        void displayVideos(List<YoutubeVideoViewModel> listViewViewModelList);

        void goToDetailsView(YoutubeVideoViewModel youtubeVideoViewModel);
    }
}
