package com.example.and_project.ui.list_view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.and_project.R;
import com.example.and_project.ui.details.fragment.VideoDetailsFragment;
import com.example.and_project.ui.list_view.adapter.ListViewAdapter;
import com.example.and_project.ui.utils.SameSearchActivity;
import com.example.and_project.ui.utils.YoutubeVideoElementAction;
import com.example.and_project.ui.utils.YoutubeVideoPresenter;
import com.example.and_project.ui.utils.YoutubeVideoSearchContract;
import com.example.and_project.ui.utils.YoutubeVideoViewModel;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ListViewFragment extends Fragment implements YoutubeVideoElementAction, YoutubeVideoSearchContract.View {

    private ListViewAdapter listViewAdapter;
    private RecyclerView recyclerView;
    private SearchView searchView;
    private ProgressBar progressBar;
    private View rootView;
    private YoutubeVideoSearchContract.Presenter youtubeVideoPresenter;
    private SameSearchActivity activity;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (SameSearchActivity) context;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_list_view, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        progressBar = rootView.findViewById(R.id.progress_bar);
        youtubeVideoPresenter = new YoutubeVideoPresenter();
        youtubeVideoPresenter.attachview(this);
        setupSearchView();
        setupRecyclerView();

    }

    private void setupSearchView() {
        searchView = rootView.findViewById(R.id.search_view);

        String lastSearch = activity.getSearchKeywords();
        if (lastSearch != null && !lastSearch.equals("")) {
            searchView.setQuery(lastSearch, false);
            searchView.clearFocus();
            youtubeVideoPresenter.searchVideos(lastSearch);
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            private Timer timer = new Timer();

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String s) {
                if (s.length() == 0) {
                    youtubeVideoPresenter.cancelSubscription();
                    progressBar.setVisibility(View.GONE);
                    listViewAdapter.clear();
                    activity.setSearchKeywords(s);
                    searchView.clearFocus();
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    timer.cancel();
                    timer = new Timer();
                    int sleep = 350;
                    if (s.length() == 1)
                        sleep = 5000;
                    else if (s.length() <= 3)
                        sleep = 300;
                    else if (s.length() <= 5)
                        sleep = 200;
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            youtubeVideoPresenter.searchVideos(s);
                            activity.setSearchKeywords(s);
                        }
                    }, sleep);
                }
                return true;
            }
        });
    }

    private void setupRecyclerView() {
        recyclerView = rootView.findViewById(R.id.recycler_view);
        listViewAdapter = new ListViewAdapter(this);
        recyclerView.setAdapter(listViewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    public void onVideoClicked(String youtubeVideoId) {
        youtubeVideoPresenter.getDetailsOnClicked(youtubeVideoId);
    }

    public void goToDetailsView(YoutubeVideoViewModel youtubeVideoViewModel) {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment f = VideoDetailsFragment.newInstance(youtubeVideoViewModel);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.nav_host_fragment, f);
        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();
    }

    public void displayVideos(List<YoutubeVideoViewModel> listViewViewModelList) {
        progressBar.setVisibility(View.GONE);
        listViewAdapter.bindViewModels(listViewViewModelList);
    }

    public void onDestroy() {
        super.onDestroy();
        youtubeVideoPresenter.detachView();
    }
}