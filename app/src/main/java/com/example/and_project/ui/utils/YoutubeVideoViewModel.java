package com.example.and_project.ui.utils;

import java.util.Date;

/**
 * Model used by every recycler view adapter in the application.
 */
public class YoutubeVideoViewModel {
    private String videoThumbnailUrl;
    private String videoHighThumbnailUrl;
    private String videoTitle;
    private String videoChannel;
    private String videoYoutubeId;
    private Date publishedAt;
    private String duration;
    private long viewCount;
    private long likeCount;
    private long dislikeCount;
    private long commentCount;

    public String getVideoThumbnailUrl() {
        return videoThumbnailUrl;
    }

    public void setVideoThumbnailUrl(String videoThumbnailUrl) {
        this.videoThumbnailUrl = videoThumbnailUrl;
    }

    public String getVideoHighThumbnailUrl() {
        return videoHighThumbnailUrl;
    }

    public void setVideoHighThumbnailUrl(String videoHighThumbnailUrl) {
        this.videoHighThumbnailUrl = videoHighThumbnailUrl;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public String getVideoChannel() {
        return videoChannel;
    }

    public void setVideoChannel(String videoChannel) {
        this.videoChannel = videoChannel;
    }

    public String getVideoYoutubeId() {
        return videoYoutubeId;
    }

    public void setVideoYoutubeId(String videoYoutubeId) {
        this.videoYoutubeId = videoYoutubeId;
    }

    public Date getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(Date publishedAt) {
        this.publishedAt = publishedAt;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public long getViewCount() {
        return viewCount;
    }

    public void setViewCount(long viewCount) {
        this.viewCount = viewCount;
    }

    public long getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(long likeCount) {
        this.likeCount = likeCount;
    }

    public long getDislikeCount() {
        return dislikeCount;
    }

    public void setDislikeCount(long dislikeCount) {
        this.dislikeCount = dislikeCount;
    }

    public long getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(long commentCount) {
        this.commentCount = commentCount;
    }
}
