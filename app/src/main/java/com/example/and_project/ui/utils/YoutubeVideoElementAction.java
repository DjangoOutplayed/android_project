package com.example.and_project.ui.utils;

/**
 * Interface used by List view, Grid view and Watch Later view to make list items a link
 * to video details.
 */
public interface YoutubeVideoElementAction {
    void onVideoClicked(String videoYoutubeId);
}
