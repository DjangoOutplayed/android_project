package com.example.and_project.ui.grid_view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.and_project.R;
import com.example.and_project.ui.utils.YoutubeVideoElementAction;
import com.example.and_project.ui.utils.YoutubeVideoViewModel;

import java.util.ArrayList;
import java.util.List;

public class GridViewAdapter extends RecyclerView.Adapter<GridViewAdapter.GridViewHolder> {
    public static class GridViewHolder extends RecyclerView.ViewHolder {
        private TextView videoTitleTextView;
        private ImageView videoIconImageView;
        private View v;
        private YoutubeVideoViewModel youtubeVideoViewModel;
        private YoutubeVideoElementAction youtubeVideoElementAction;

        public GridViewHolder(View v, YoutubeVideoElementAction youtubeVideoElementAction) {
            super(v);
            this.v = v;
            videoTitleTextView = v.findViewById(R.id.video_title_textview);
            videoIconImageView = v.findViewById(R.id.video_image_preview);
            this.youtubeVideoElementAction = youtubeVideoElementAction;
            setupListeners();
        }

        private void setupListeners() {
            v.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    youtubeVideoElementAction.onVideoClicked(youtubeVideoViewModel.getVideoYoutubeId());
                }
            });
        }

        void bind(YoutubeVideoViewModel youtubeVideoViewModel) {
            this.youtubeVideoViewModel = youtubeVideoViewModel;
            videoTitleTextView.setText(youtubeVideoViewModel.getVideoTitle());
            Glide.with(v)
                    .load(youtubeVideoViewModel.getVideoHighThumbnailUrl())
                    .into(videoIconImageView);
        }
    }

    private List<YoutubeVideoViewModel> youtubeVideoViewModelList;
    private YoutubeVideoElementAction youtubeVideoElementAction;

    public GridViewAdapter(YoutubeVideoElementAction youtubeVideoElementAction) {
        this.youtubeVideoViewModelList = new ArrayList<>();
        this.youtubeVideoElementAction = youtubeVideoElementAction;
    }

    public void bindViewModels(List<YoutubeVideoViewModel> youtubeVideoViewModels) {
        this.youtubeVideoViewModelList.clear();
        this.youtubeVideoViewModelList.addAll(youtubeVideoViewModels);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public GridViewAdapter.GridViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_grid_view, parent, false);
        return new GridViewAdapter.GridViewHolder(v, youtubeVideoElementAction);
    }

    @Override
    public void onBindViewHolder(@NonNull GridViewAdapter.GridViewHolder holder, int position) {
        holder.bind(youtubeVideoViewModelList.get(position));
    }

    @Override
    public int getItemCount() {
        return youtubeVideoViewModelList.size();
    }

    public void clear() {
        youtubeVideoViewModelList.clear();
    }
}
