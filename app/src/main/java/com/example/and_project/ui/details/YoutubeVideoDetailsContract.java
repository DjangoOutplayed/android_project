package com.example.and_project.ui.details;

import com.example.and_project.ui.utils.YoutubeVideoViewModel;

public interface YoutubeVideoDetailsContract {
    interface Presenter {
        void attachview(View view);

        void detachView();

        void cancelSubscription();

        void toggleWatchLaterState(boolean isWatchLater, YoutubeVideoViewModel youtubeVideoViewModel);

        void setIconWithWatchLaterState(String youtubeVideoId);
    }

    interface View {
        void toggleIconOnFavButton(boolean isWatchLater);
    }
}
