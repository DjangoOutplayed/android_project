package com.example.and_project.ui.watch_later.presenter;

import com.example.and_project.data.api.model.YoutubeDetailsResponse;
import com.example.and_project.data.api.model.YoutubeVideoWithOneId;
import com.example.and_project.data.db.entities.YoutubeVideoEntity;
import com.example.and_project.data.di.FakeDependencyInjection;
import com.example.and_project.data.repository.youtube_videos.YoutubeVideoDataRepository;
import com.example.and_project.ui.utils.YoutubeVideoEntityToYoutubeVideoViewModelMapper;
import com.example.and_project.ui.utils.YoutubeVideoToYoutubeVideoViewModelMapper;
import com.example.and_project.ui.utils.YoutubeVideoViewModel;
import com.example.and_project.ui.watch_later.YoutubeVideoWatchLaterContract;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

public class YoutubeVideoWatchLaterPresenter implements YoutubeVideoWatchLaterContract.Presenter {
    public YoutubeVideoWatchLaterContract.View view;
    private CompositeDisposable cd;
    public YoutubeVideoDataRepository repository;

    public YoutubeVideoWatchLaterPresenter() {
        cd = new CompositeDisposable();
        repository = FakeDependencyInjection.getYoutubeVideoDataRepository();
    }

    public void attachview(YoutubeVideoWatchLaterContract.View view) {
        this.view = view;
    }

    public void detachView() {
        this.view = null;
        cd.dispose();
    }

    public void cancelSubscription() {
        cd.clear();
    }

    public void getWatchLaterVideos() {
        Flowable<List<YoutubeVideoEntity>> favoriteFlowable = repository.getAllWatchLaterVideos();

        final YoutubeVideoEntityToYoutubeVideoViewModelMapper mapper = new YoutubeVideoEntityToYoutubeVideoViewModelMapper();

        Disposable disposable = favoriteFlowable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSubscriber<List<YoutubeVideoEntity>>() {
                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onNext(List<YoutubeVideoEntity> videos) {
                        List<YoutubeVideoViewModel> mappedResponse = mapper.map(videos);
                        view.displayVideos(mappedResponse);
                    }

                    @Override
                    public void onError(Throwable t) {
                        System.out.println(t.getMessage());
                        for (StackTraceElement element : t.getStackTrace()
                        ) {
                            System.out.println(element);
                        }
                    }
                });

        cd.add(disposable);
    }

    public void getDetailsOnClicked(String youtubeVideoId) {
        Single<YoutubeDetailsResponse> youtubeVideoSingle = repository.getVideoDetails(youtubeVideoId);

        final YoutubeVideoToYoutubeVideoViewModelMapper mapper = new YoutubeVideoToYoutubeVideoViewModelMapper();

        Disposable disposable = youtubeVideoSingle
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<YoutubeDetailsResponse>() {
                    @Override
                    public void onSuccess(YoutubeDetailsResponse youtubeDetailsResponse) {
                        YoutubeVideoWithOneId youtubeVideo = youtubeDetailsResponse.getYoutubeVideoDetails();
                        YoutubeVideoViewModel youtubeVideoViewModel = mapper.map(youtubeVideo);

                        view.goToDetailsView(youtubeVideoViewModel);
                    }

                    @Override
                    public void onError(Throwable e) {
                        System.out.println(e.getMessage());
                        for (StackTraceElement element : e.getStackTrace()
                        ) {
                            System.out.println(element);
                        }
                    }
                });
    }
}
